import styled from "styled-components";


export const Container = styled.div`
    display              : grid;
    grid-template-columns: repeat(4, 1fr);
`;

export const Card = styled.div`
    width          : 400px;
    height         : 400px;
    border-radius  : 50%;
    border         : 1px solid black;
    margin         : 1rem auto;
    display        : flex;
    align-items    : center;
    justify-content: center;
`;

export const Ul = styled.ul`
    position: relative;
    width   : 280px;
    height  : 280px;
`;

export const Picture = styled.li`
    list-style: none;
    position  : absolute;
    top       : 50%;
    left      : 50%;
    margin: -50px;
    
    &:nth-of-type(1){
        transform: rotate(0deg) translate(140px) rotate(-45deg);
        
        img{
            width: 85px;
        }
    }
    &:nth-of-type(2){
        transform: rotate(45deg) translate(140px) rotate(-45deg);
    }
    &:nth-of-type(3){
        transform: rotate(99deg) translate(140px) rotate(266deg);
    }
    &:nth-of-type(4){
        transform: rotate(151deg) translate(140px) rotate(-185deg);
    }
    &:nth-of-type(5){
        transform: rotate(20deg) translate(-140px) rotate(318deg);
    }
    &:nth-of-type(6){
        transform: rotate(245deg) translate(140px) rotate(-245deg);
    }
    &:nth-of-type(7){
        transform: rotate(310deg) translate(140px) rotate(-310deg);
    }

    img{
        border-radius: 50%;
    }
`;