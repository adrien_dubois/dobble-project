import { Card, Container, Picture, Ul } from './Cards.elements';
import { numberToImageMap } from './numberToImageMap';


function App() {

  var N = 8;     // nombre de symbole sur chaque carte
  var nC = 0;    // nombre progressif de cartes
  var sTot = []; // tableau de cartes
  
  // check si N est valide (nombre positif)
  if (!isPrime(N-1)) {
	document.write("<pre>ERROR: N value ("+N+") is not a prime number +1:"); 
	document.write(" some tests will fail.</pre>");
  }
  
  for (let i=0; i <= N-1; i++)  {
    var s = [];
    nC++;
    s.push(1);
    for (let i2=1; i2 <= N-1; i2++) {
        s.push((N-1) + (N-1) * (i-1) + (i2+1));
    }
    sTot.push(s);
  }
  
  // Génère une carte à partir de #N+1 to #N+(N-1)*(N-1)
  for (let i= 1; i<= N-1; i++) {
    for (let i2=1; i2 <= N-1; i2++) {
      var card = [];
      nC++;
      card.push(i+1);
      for (let i3=1; i3<= N-1; i3++) {
        card.push((N+1) + (N-1) * (i3-1) + ( ((i-1) * (i3-1) + (i2-1)) ) % (N-1));
      }
      sTot.push(card);
    }
  }
  
  
  
/**
 * Test si le nombre est positif
 * @param num 
 * @returns 
 */
function isPrime(num: number) {
  for(var i = 2; i < num; i++) {
    if(num % i === 0) return false;
  }
  return num > 1;
}

function pad(n: any, width: any, z?: any) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

/**
 * Construction des cartes en tabbleau
 * @param sTot 
 * @returns 
 */
function outputSeries(sTot: any[], imageToMap: any) {
  var nPad = sTot.length.toString().length;
  var cnt = 0;

  const rows = []

  for (var i in sTot) {
    cnt++;
    var sLog = "";
    for (var i2 in sTot[i]) {
      // sLog += pad(sTot[i][i2], nPad) + " ";
      var numberString: number = pad(sTot[i][i2], 2);
      sLog += `${imageToMap[numberString]}`; 
    }

    var singleCard = sLog.split(',');

    // Enlever l'espace supplémentaire de l'array
    singleCard.length = 8

    rows.push(singleCard)
    // rows.push(sLog)
  }

  return rows;
}

// retourne la série à l'écran
const totalCards =  outputSeries(sTot, numberToImageMap);

console.log(totalCards)

  return (
    <>
      <div>
        <h1>DOBBLE CARDS</h1>
      </div>
      
      <Container>

        { totalCards.map((card, idx) => (
          
          <Card key={idx}>
            <Ul>
              {card.map((image, idx) =>{ 

              return (
                <Picture 
                  key={idx} 
                >
                  <img src={image} />
                </Picture>
              )
            })}
            </Ul>

          </Card>
          
        )) }
      </Container>
    </>
  );
}

export default App;
