import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`

    :root{
        --circle-size: 280px;
        --item-size: 100px;
        --item-count: 8;
        --angle: (360 / 8);
    }

    *{
        padding: 0;
        margin: 0;
    }
`;

export default GlobalStyle;